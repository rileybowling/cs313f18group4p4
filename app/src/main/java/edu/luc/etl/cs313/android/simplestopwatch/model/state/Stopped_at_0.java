package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class Stopped_at_0 implements StopwatchState {

    public Stopped_at_0(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionStart();
        sm.actionInc();
        sm.toStopped_at_int();
    }

    @Override
    public void onTick() {
        sm.actionReset();
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPEDAT0;
    }
}
