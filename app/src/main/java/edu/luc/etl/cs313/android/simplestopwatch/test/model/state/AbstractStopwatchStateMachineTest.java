package edu.luc.etl.cs313.android.simplestopwatch.test.model.state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.StopwatchUIUpdateListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simplestopwatch.model.clock.OnTickListener;
import edu.luc.etl.cs313.android.simplestopwatch.model.state.StopwatchStateMachine;
import edu.luc.etl.cs313.android.simplestopwatch.model.time.TimeModel;

/**
 * Testcase superclass for the stopwatch state machine model. Unit-tests the state
 * machine in fast-forward mode by directly triggering successive tick events
 * without the presence of a pseudo-real-time clock. Uses a single unified mock
 * object for all dependencies of the state machine model.
 *
 * @author laufer
 * @see ://xunitpatterns.com/Testcase%20Superclass.html
 */
public abstract class AbstractStopwatchStateMachineTest {

    private StopwatchStateMachine model;

    private UnifiedMockDependency dependency;

    @Before
    public void setUp() throws Exception {
        dependency = new UnifiedMockDependency();
    }

    @After
    public void tearDown() {
        dependency = null;
    }

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final StopwatchStateMachine model) {
        this.model = model;
        if (model == null)
            return;
        this.model.setUIUpdateListener(dependency);
        this.model.actionInit();
    }

    protected UnifiedMockDependency getDependency() {
        return dependency;
    }

    /**
     * Verifies that we're initially in the stopped state (and told the listener
     * about it).
     */
    @Test
    public void testPreconditions() {
        assertEquals(R.string.STOPPEDAT0, dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press start,
     * expect state to change to running after 3 tick events.
     */
    @Test
    public void testThreeSeconds() {
        assertTimeEquals(0);
        // directly invoke the button press event handler methods
        model.onStartStop();
        onTickRepeat(3);
        assertEquals(R.string.RUNNING,dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press start,
     * press button 99 times (goes up to 99 seconds), expect state to change to running at that point.
     */
    @Test
    public void testMaximum(){
        assertTimeEquals(0);
        for (int i=0; i<99; i++) {
            model.onStartStop();
        }
        assertEquals(R.string.RUNNING,dependency.getState());
    }

    /**
     * Verifies the following scenario: state is stopped at 0 seconds, press start,
     * expect time to be incremented by 1.
     */
    @Test
    public void testButtonIncrement(){
        assertEquals(R.string.STOPPEDAT0, dependency.getState());
        model.onStartStop();
        assertEquals(1,model.getTime());
    }

    /**
     * Verifies the following scenario: time is 0, press button 10 times (so time is at 10 seconds), wait for 3 tick events so timer starts running
     * expect state to change to stopped at 0 when stop button pressed.
     */
    @Test
    public void testStopButton(){
        assertTimeEquals(0);
        for (int i=0; i<10; i++){
            model.onStartStop();
        }
        onTickRepeat(3);
        model.onStartStop();
        assertEquals(R.string.STOPPEDAT0,dependency.getState());
    }

    /**
     * Verifies the following scenario: time is 0, press button 10 times (so time is at 10 seconds),
     * wait 3 seconds for timer to start running and 10 seconds for timer to reach 0,
     * expect state to change to alarm beeping and then to stopped at 0 when button pressed.
     */
    @Test
    public void testAlarmBeepingStartStop(){
        assertTimeEquals(0);
        for (int i=0; i<10; i++){
            model.onStartStop();
        }
        onTickRepeat(13);
        assertEquals(R.string.BEEPING, dependency.getState());
        model.onStartStop();
        assertEquals(R.string.STOPPEDAT0, dependency.getState());
    }



    /**
     * Sends the given number of tick events to the model.
     *
     *  @param n the number of tick events
     */
    protected void onTickRepeat(final int n) {
        for (int i = 0; i < n; i++)
            model.onTick();
    }

    /**
     * Checks whether the model has invoked the expected time-keeping
     * methods on the mock object.
     */
    protected void assertTimeEquals(final int t) {
        assertEquals(t, dependency.getTime());
    }
}

/**
 * Manually implemented mock object that unifies the three dependencies of the
 * stopwatch state machine model. The three dependencies correspond to the three
 * interfaces this mock object implements.
 *
 * @author laufer
 */
class UnifiedMockDependency implements TimeModel, ClockModel, StopwatchUIUpdateListener {

    private int timeValue = -1, stateId = -1;

    private int runningTime = 0, lapTime = -1;

    private boolean started = false;

    public int getTime() {
        return timeValue;
    }

    public int getState() {
        return stateId;
    }

    public boolean isStarted() {
        return started;
    }

    @Override
    public void updateTime(final int timeValue) {
        this.timeValue = timeValue;
    }

    @Override
    public void updateState(final int stateId) {
        this.stateId = stateId;
    }

    @Override
    public void setOnTickListener(OnTickListener listener) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void start() {
        started = true;
    }

    @Override
    public void stop() {
        started = false;
    }

    @Override
    public void resetRuntime() {
        runningTime = 0;
    }

    @Override
    public void incRuntime() {
        runningTime++;
    }

    @Override
    public int getRuntime() {
        return runningTime;
    }

    @Override
    public void decRuntime() {
        runningTime--;
    }
}
