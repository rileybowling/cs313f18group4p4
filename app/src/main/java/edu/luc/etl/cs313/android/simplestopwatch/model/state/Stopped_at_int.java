package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.common.Constants;

class Stopped_at_int implements StopwatchState {

    public Stopped_at_int(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private int countdown = 0;

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionInc();
        countdown = 0;
        if(sm.getTime() >= Constants.MAXTIME){ sm.toRunningState();}
    }

    @Override
    public void onTick() {
        if(countdown == 2) {
            sm.toRunningState();
        }
        countdown++;
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }
}
