[1mdiff --git a/app/src/main/java/edu/luc/etl/cs313/android/simplestopwatch/test/android/AbstractStopwatchActivityTest.java b/app/src/main/java/edu/luc/etl/cs313/android/simplestopwatch/test/android/AbstractStopwatchActivityTest.java[m
[1mindex 37725ad..feeaede 100644[m
[1m--- a/app/src/main/java/edu/luc/etl/cs313/android/simplestopwatch/test/android/AbstractStopwatchActivityTest.java[m
[1m+++ b/app/src/main/java/edu/luc/etl/cs313/android/simplestopwatch/test/android/AbstractStopwatchActivityTest.java[m
[36m@@ -43,7 +43,7 @@[m [mpublic abstract class AbstractStopwatchActivityTest {[m
     }[m
 [m
     /**[m
[31m-     * Verifies the following scenario: time is 0, press start, wait 5+ seconds, expect time 5.[m
[32m+[m[32m     * Verifies the following scenario: time is 0, press start, wait 1 second, expect time 1.[m
      *[m
      * @throws Throwable[m
      */[m
[36m@@ -53,7 +53,7 @@[m [mpublic abstract class AbstractStopwatchActivityTest {[m
             assertEquals(0, getDisplayedValue());[m
             assertTrue(getStartStopButton().performClick());[m
         });[m
[31m-        Thread.sleep(5100); // <-- do not run this in the UI thread![m
[32m+[m[32m        Thread.sleep(1000); // <-- do not run this in the UI thread![m
         runUiThreadTasks();[m
         getActivity().runOnUiThread(() -> {[m
             assertEquals(1, getDisplayedValue());[m
[36m@@ -61,6 +61,25 @@[m [mpublic abstract class AbstractStopwatchActivityTest {[m
         });[m
     }[m
 [m
[32m+[m[32m    /**[m
[32m+[m[32m     * Verifies the following scenario: time is 0, press start, wait 5 seconds, expect time 0.[m
[32m+[m[32m     *[m
[32m+[m[32m     * @throws Throwable[m
[32m+[m[32m     */[m
[32m+[m[32m    @Test[m
[32m+[m[32m    public void testActivityScenarioRun2() throws Throwable {[m
[32m+[m[32m        getActivity().runOnUiThread(() -> {[m
[32m+[m[32m            assertEquals(0, getDisplayedValue());[m
[32m+[m[32m            assertTrue(getStartStopButton().performClick());[m
[32m+[m[32m        });[m
[32m+[m[32m        Thread.sleep(5000); // <-- do not run this in the UI thread![m
[32m+[m[32m        runUiThreadTasks();[m
[32m+[m[32m        getActivity().runOnUiThread(() -> {[m
[32m+[m[32m            assertEquals(0, getDisplayedValue());[m
[32m+[m[32m            assertTrue(getStartStopButton().performClick());[m
[32m+[m[32m        });[m
[32m+[m[32m    }[m
[32m+[m
     /**[m
      * Tests that the values don't change after rotation[m
      *[m
